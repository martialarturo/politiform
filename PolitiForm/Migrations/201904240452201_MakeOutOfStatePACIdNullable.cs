namespace PolitiForm.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class MakeOutOfStatePACIdNullable : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Contributors", "OutOfStatePACId", c => c.Int());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Contributors", "OutOfStatePACId", c => c.Int(nullable: false));
        }
    }
}
