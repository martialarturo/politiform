namespace PolitiForm.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class PopulateContributors : DbMigration
    {
        public override void Up()
        {
            Sql("INSERT INTO Contributors (Name, OutOfStatePAC, ContributorAddressSt, ContributorAddressCity, ContributorAddressState, ContributorAddressZip) VALUES ('Michael Fisher', 'false', '7306 SW 34th Ave', 'Amarillo', 'TX', 79121)");
            Sql("INSERT INTO Contributors (Name, OutOfStatePAC, ContributorAddressSt, ContributorAddressCity, ContributorAddressState, ContributorAddressZip) VALUES ('Claudette Smith', 'false', '7306 SW 34th Ave', 'Amarillo', 'TX', 79121)");
        }

        public override void Down()
        {
        }
    }
}
