namespace PolitiForm.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ContributionModelUpdated1 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Contributions",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Date = c.DateTime(nullable: false),
                        ContributorId = c.Int(nullable: false),
                        ContributionAmount = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Contributors", t => t.ContributorId, cascadeDelete: true)
                .Index(t => t.ContributorId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Contributions", "ContributorId", "dbo.Contributors");
            DropIndex("dbo.Contributions", new[] { "ContributorId" });
            DropTable("dbo.Contributions");
        }
    }
}
