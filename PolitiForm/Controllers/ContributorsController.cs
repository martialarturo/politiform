﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PolitiForm.Models;

namespace PolitiForm.Controllers
{
    public class ContributorsController : Controller
    {
        // GET: Contributors
        public ActionResult Index(int? pageIndex, string sortBy)
        {
            if (!pageIndex.HasValue)
                pageIndex = 1;

            if (String.IsNullOrWhiteSpace(sortBy))
                sortBy = "name";

            return Content(String.Format("pageIndex={0}&sortBy={1}", pageIndex, sortBy));
        }

        public ActionResult Edit(int Id)
        {
            return Content("id=" + Id);
        }
    }
}