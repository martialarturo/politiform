﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(PolitiForm.Startup))]
namespace PolitiForm
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
