﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PolitiForm.Models
{
    public class Contributor
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public bool OutOfStatePAC { get; set; }
        public int? OutOfStatePACId { get; set; }
        public string ContributorAddressSt { get; set; }
        public string ContributorAddressCity { get; set; }
        public string ContributorAddressState { get; set; }
        public string ContributorAddressZip { get; set; }
        public string ContributorJob { get; set; }
        public string Employer { get; set; }
    }
}