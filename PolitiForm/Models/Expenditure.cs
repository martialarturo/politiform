﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PolitiForm.Models
{
    public class Expenditure
    {
        public int Id { get; set; }
        public DateTime Date { get; set; }
        public Vendor Vendor { get; set; }
        public int ContributionAmount { get; set; }
    }
}