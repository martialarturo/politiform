﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using PolitiForm.Models;

namespace PolitiForm.Models
{
    public class Contribution
    {
        public int Id { get; set; }
        public DateTime Date { get; set; }
        public Contributor Contributor { get; set; }
        public int ContributorId { get; set; }
        public int ContributionAmount { get; set; }
    }
}